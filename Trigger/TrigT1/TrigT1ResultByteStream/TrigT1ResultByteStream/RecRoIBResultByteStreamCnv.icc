// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGT1RESULTBYTESTREAM_RECROIBRESULTBYTESTREAMCNV_ICC
#define TRIGT1RESULTBYTESTREAM_RECROIBRESULTBYTESTREAMCNV_ICC

// Gaudi/Athena include(s):
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/DataObject.h"

#include "ByteStreamCnvSvc/ByteStreamCnvSvc.h"
#include "ByteStreamCnvSvcBase/ByteStreamAddressL1R.h"

#include "SGTools/StorableConversions.h"

// Trigger include(s):
#include "TrigT1Result/RecRoIBResult.h"

// Local include(s):
#include "TrigT1ResultByteStream/L1SrcIdMap.h"

/**
 * The constructor sets up the ToolHandle object(s) and initialises the
 * base class in the correct way.
 */
template< class ROBF >
RecRoIBResultByteStreamCnv< ROBF >::RecRoIBResultByteStreamCnv( ISvcLocator* svcloc )
  : Converter( ByteStream_StorageType, classID(), svcloc ),
    m_tool( "RecRoIBResultByteStreamTool" ) {

}

/**
 * Function telling the framework the Class ID of the object that this converter
 * is for (RecRoIBResult).
 */
template< class ROBF >
const CLID& RecRoIBResultByteStreamCnv< ROBF >::classID() {

  return ClassID_traits<ROIB::RecRoIBResult>::ID();

}


/**
 * Init method gets all necessary services etc.
 */
template< class ROBF >
StatusCode RecRoIBResultByteStreamCnv< ROBF >::initialize() {

  //
  // Initialise the base class:
  //
  ATH_CHECK(  Converter::initialize() );

  MsgStream log( msgSvc(), "RecRoIBResultByteStreamCnv" );
  log << MSG::DEBUG << "In initialize()" << endmsg;

  //
  // Get RecRoIBResultByteStreamTool:
  //
  ATH_CHECK( m_tool.retrieve() );
  log << MSG::DEBUG << "Retrieved RecRoIBResultByteStreamTool" << endmsg;

  return StatusCode::SUCCESS;

}

/**
 * This function creates the RecRoIBResult object from the BS data. It collects all the
 * ROB fragments coming from the RoI Builder and gives them to RecRoIBResultByteStreamTool
 * for conversion.
 */
template< class ROBF >
StatusCode RecRoIBResultByteStreamCnv< ROBF >::createObj( IOpaqueAddress* pAddr, DataObject*& pObj ) {

  MsgStream log( msgSvc(), "RecRoIBResultByteStreamCnv" );
  log << MSG::DEBUG << "createObj() called" << endmsg;

  ByteStreamAddressL1R *pAddrL1;
  pAddrL1 = dynamic_cast< ByteStreamAddressL1R* >( pAddr );
  if( ! pAddrL1 ) {
    log << MSG::ERROR << " Cannot cast to ByteStreamAddressL1R" << endmsg ;
    return StatusCode::FAILURE;
  }

  ROIB::RecRoIBResult* result;

  const std::vector< ROBF >& robs = pAddrL1->getPointers();

  // Convert to Object
  ATH_CHECK(  m_tool->convert( robs, result ) );
  log << MSG::DEBUG << " Created Objects: " << *( pAddrL1->par() ) << endmsg;

  pObj = SG::asStorable( result );
  return StatusCode::SUCCESS;

}

#endif // TRIGT1RESULTBYTESTREAM_RECROIBRESULTBYTESTREAMCNV_ICC
